#include "syscall.h"

unsigned int syscall_tbl[17];

void
lol()
{
  write("GETPAL", 6);
}

void
lol2()
{
  write("SETPAL", 6);
}

void
init_syscall_tbl()
{
  syscall_tbl[0]  = 0;
  syscall_tbl[1]  = (unsigned int) write;
  syscall_tbl[2]  = (unsigned int) sbrk;
  syscall_tbl[3]  = (unsigned int) getkey;
  syscall_tbl[4]  = (unsigned int) gettick;
  syscall_tbl[5]  = (unsigned int) open;
  syscall_tbl[6]  = (unsigned int) read;
  syscall_tbl[7]  = (unsigned int) seek;
  syscall_tbl[8]  = (unsigned int) close;
  syscall_tbl[9]  = (unsigned int) setvideo;
  syscall_tbl[10] = (unsigned int) swap_frontbuffer;
  syscall_tbl[11] = (unsigned int) lol;
  syscall_tbl[12] = (unsigned int) lol2;
  syscall_tbl[13] = (unsigned int) lol;
  syscall_tbl[14] = (unsigned int) lol;
  syscall_tbl[15] = (unsigned int) send;
  syscall_tbl[16] = (unsigned int) recv;
}
