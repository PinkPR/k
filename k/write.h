#ifndef WRITE_H
# define WRITE_H

# include "kstd.h"
# include "serial_driver.h"
# include "asm_utils.h"

# define VIDEO_MEM    ((char*) 0xB8000)
# define W_COLOR      ((char) (CONS_BLACK << 4) | CONS_WHITE)
# define MAX_COL      80
# define MAX_LINE     25
# define TAB_SPACE    8

void clear_screen();
void my_memcpy(char *dest, const char *src, size_t len);

#endif /* WRITE_H */
