#ifndef PIC_H
# define PIC_H

# include "serial_driver.h"

# define  PIC1A 0x20
# define  PIC1B 0x21
# define  PIC2A 0xA0
# define  PIC2B 0xA1

void init_ocw(int mask, int port);
void init_pic(int offset1, int offset2);

#endif /* PIC_H */
