#include "serial_driver.h"
#include "libvga_io.h"
#include "asm_utils.h"

void outb(unsigned short port, unsigned short val)
{
  __asm__ volatile ("outb %0, %1" : : "a"(val), "d"(port));
}

int inb(unsigned short port)
{
  unsigned char volatile in = 0;

  __asm__ volatile ("inb %1, %0" : "=Na"(in) : "d"(port));

  return in;
}

void init_serial(int port)
{
  outb(port + 1, 0x01); //Enable interrupts
  outb(port + 3, 0x80); //Enable DLAB
  outb(port + 0, 0x03); //Set Baud divisor to 3 (38400)
  outb(port + 1, 0x00); //most significant byte of baud divisor
  outb(port + 3, 0x03); //8 bits, no parity, one stop bit
  //outb(port + 2, 0xC7);
  //outb(port + 4, 0x0B);
}

int recv(int port, void* data, unsigned int len)
{
  int l = len;

  for (; len; data++, len--)
    *((char*) data) = inb(port);

  return l;
}

int send(int port, void* data, unsigned int len)
{
  int l = len;

  for (; len; data++, len--)
    outb(port, *((char*) data));

  return l;
}
