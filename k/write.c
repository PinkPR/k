#include "write.h"

int column = 0;
int line = 0;

void clear_screen()
{
  char* video_mem = VIDEO_MEM;

  for (int i = 0; i < MAX_LINE; i++)
  {
    for (int j = 0; j < MAX_COL; j++)
    {
      video_mem[(i * MAX_COL + j) * 2] = ' ';
      video_mem[(i * MAX_COL + j) * 2 + 1] = W_COLOR;
    }
  }
}

void my_memcpy(char* dest, const char* src, size_t len)
{
  for (; len; len--, dest++, src++)
    *dest = *src;
}

void clear_line(int l)
{
  char* mem = VIDEO_MEM;

  for (int i = 0; i < MAX_COL; i++)
  {
    mem[(l * MAX_COL + i) * 2] = ' ';
    mem[(l * MAX_COL + i) * 2 + 1] = W_COLOR;
  }
}

int write_char(char c)
{
  char* video_mem = VIDEO_MEM;

  if (c == '\n')
  {
    column = 0;
    line++;
  }
  else if (c == '\r')
  {
    column = 0;
  }
  else if (c == '\t')
  {
    column++;

    while (column % TAB_SPACE)
    {
      column++;
    }

    if (column >= MAX_COL)
      line++;

    column %= MAX_COL;
  }
  else
  {
    video_mem[(line * MAX_COL + column) * 2] = c;
    video_mem[(line * MAX_COL + column) * 2 + 1] = W_COLOR;
    column++;
  }

  if (column >= MAX_COL)
  {
    line++;
    column %= MAX_COL;
  }

  if (line >= MAX_LINE)
  {
    line = MAX_LINE - 1;
    my_memcpy(VIDEO_MEM,
              (char*) VIDEO_MEM + MAX_COL * 2,
              (MAX_LINE - 1) * MAX_COL * 2);
    clear_line(MAX_LINE - 1);
  }

  return 1;
}

int write(const char* buf, size_t count)
{
  int i = count;
  init_serial(0x03f8);
  send(0x03f8, (void*) buf, count);

  for (; count; buf++, count--)
  {
    write_char(*buf);
  }

  return i;
}
