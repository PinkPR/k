#ifndef KEYBOARD_H
# define KEYBOARD_H

# include "idt.h"
# include "write.h"
# include "serial_driver.h"
# include "getkey.h"

# define KBD_BUFF_SIZE  10
# define KBD_BUFF_PORT  0x60

struct kbd_buff
{
  int size;
  int buff[10];
  int mark;
};

void  kbd_buff_init(struct kbd_buff *buff);
void  kbd_buff_push(struct kbd_buff *buff, int code);
int   kbd_buff_pop(struct kbd_buff *buff);

void  kbd_hdl(void);

#endif /* KEYBOARD_H */
