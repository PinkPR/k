#include "vga.h"
#include <stdio.h>

int
setvideo(int mode)
{
  if (mode != VIDEO_GRAPHIC && mode != VIDEO_TEXT)
  {
    return -1;
  }

  if (mode == VIDEO_GRAPHIC)
    libvga_switch_mode13h();
  else
    libvga_switch_mode3h();

  return 0;
}

void
swap_frontbuffer(const void *buffer)
{
  my_memcpy(libvga_get_framebuffer(), buffer, 320 * 200);
  printf("swap_buffer\n");
}
