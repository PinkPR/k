#ifndef VGADRIVER_H
# define VGADRIVER_H

# include "kstd.h"
# include "asm_utils.h"
# include "libvga.h"
# include "write.h"

int setvideo(int mode);
void swap_frontbuffer(const void *buffer);

#endif /* VGADRIVER_H */
