#ifndef MACROS_H
# define MACROS_H

typedef unsigned int    u32;
typedef unsigned short  u16;
typedef unsigned char   u8;

# define PACKED   __atribute__ ((packed))

#endif /* MACROS_H */
