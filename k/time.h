#ifndef TIME_H
# define TIME_H

void
init_pit(int hz);

void
timer_hdl(void);

void
wait(int s);

#endif /* !TIME_H */
