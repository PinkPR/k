#ifndef ASM_UTILS_H
# define ASM_UTILS_H

# define SYSCALL_RETURN(X) \
  return (X)

# define SYSCALL_FAIL(X) if (X)                 \
  {                                             \
  return -1;                                    \
  }

#endif /* ASM_UTILS_H */
