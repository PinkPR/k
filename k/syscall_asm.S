.global syscall_tbl
.global syscall_hdl_asm

syscall_hdl_asm:
  push %ds
  push %es
  push %fs
  push %gs
  push %edx
  push %ecx
  push %ebx
  call *syscall_tbl(,%eax,4)
  pop %ebx
  pop %ecx
  pop %edx
  pop %gs
  pop %fs
  pop %es
  pop %ds
  iret
