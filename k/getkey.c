#include "getkey.h"
#include "asm_utils.h"
#include <stdio.h>

struct kbd_buff buffer;

int
getkey(void)
{
  printf("GETKEY\n");
  int key = kbd_buff_pop(&buffer);

  return key;
}
