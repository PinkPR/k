#ifndef SERIAL_DRIVER_H
# define SERIAL_DRIVER_H

void init_serial(int port);
int send(int port, void* data, unsigned int len);
void outb(unsigned short port, unsigned short val);
int inb(unsigned short port);
//int recv(int port, void* data, unsigned int len);

#endif /* SERIAL_DRIVER_H */
