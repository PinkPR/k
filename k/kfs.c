#include "kfs.h"
#include "stdio.h"
#include "asm_utils.h"

struct kfs_descriptor fds[256];
struct kfs_superblock *kfs_super;

void*	memset(void* s, int c, size_t n)
{
  unsigned char* p = NULL;

  for (p = s; n > 0; n--, p++)
    *p = c;

  return (s);
}

int	strcmp(const char* s1, const char* s2)
{
  for (; *s1 == *s2 && *s1 != '\0'; s1++, s2++)
    continue;

  return (*s1 - *s2);
}

void*
jump(void *ptr, t_uint32 offset)
{
  return (char*)ptr + offset;
}

void
init_kfs(multiboot_info_t* info)
{
  kfs_super = (void *)((module_t *)info->mods_addr)[0].mod_start;
  memset(&(fds[0]), 0, 256 * sizeof (struct kfs_descriptor));
}

void
test_k(void)
{
  struct kfs_inode *cur = jump(kfs_super, kfs_super->inode_idx * KFS_BLK_SZ);
  int n = 20;
  while (cur != (void*)kfs_super && n >= 0)
  {
    cur = jump(kfs_super, cur->next_idx * KFS_BLK_SZ);
    --n;
  }
}

int
open(const char *pathname, int flags)
{
  if (*pathname == '/')
    pathname++;
  int fd = -1;
  struct kfs_inode *inode = jump(kfs_super, kfs_super->inode_idx * KFS_BLK_SZ);
  flags = flags;
  while (inode != (void*)kfs_super && strcmp(pathname, inode->filename) && strcmp(inode->filename, pathname))
  {
    inode = jump(kfs_super, inode->next_idx * KFS_BLK_SZ);
  }
  if (inode == 0)
  {
    SYSCALL_RETURN(fd);
  }
  fd = 0;

  for (; fds[fd].inode; fd++);
  fds[fd].inode = inode;
  fds[fd].offset = 0;
  printf("[open] found %s\n", inode->filename);
  SYSCALL_RETURN(fd);
}

void
read_data_block(const unsigned int begin,
                const unsigned int end,
                const unsigned int offset,
                const unsigned int fd,
                const struct kfs_block *blk,
                char *buf)
{
  unsigned int i = 0;

  if ((int) begin - (int) offset * KFS_BLK_DATA_SZ > (int) i)
    i = (int) begin - (int) offset * KFS_BLK_DATA_SZ;

  for (;
       i < KFS_BLK_DATA_SZ &&
       fds[fd].offset < end;
       i++)
  {
    buf[(int) offset * KFS_BLK_DATA_SZ - (int) begin + (int) i] = blk->data[i];
    fds[fd].offset++;
  }
}

void
read_indirect_block(const unsigned int begin,
                    const unsigned int end,
                    const unsigned int offset,
                    const unsigned int fd,
                    const struct kfs_i_block_index *i_block,
                    char* buf)
{
  unsigned int i = 0;
  void* adress = 0;

  if ((int) begin - (int) offset * KFS_BLK_DATA_SZ >= KFS_BLK_DATA_SZ)
    i = ((int) begin - (int) offset * KFS_BLK_DATA_SZ) / KFS_BLK_DATA_SZ;

  for (;
       i < i_block->blk_cnt &&
       fds[fd].offset < end;
       i++)
  {
    adress = (void*) ((int) kfs_super + (int) i_block->blks[i] * KFS_BLK_SZ);
    read_data_block(begin, end, offset + i, fd, adress, buf);
  }
}

void
read_i_node(const unsigned int begin,
            const unsigned int end,
            const unsigned int fd,
            const struct kfs_inode *inode,
            char *buf)
{
  unsigned int data_i = 0;
  unsigned int iblk_i = 0;
  void* adress = 0;

  if (begin >= KFS_BLK_DATA_SZ)
    data_i = begin / KFS_BLK_DATA_SZ;

  for (;
       data_i < inode->d_blk_cnt &&
       fds[fd].offset < end;
       data_i++)
  {
    adress = (void*) ((int) kfs_super + (int) inode->d_blks[data_i] * KFS_BLK_SZ);
    read_data_block(begin, end, data_i, fd, adress, buf);
  }

  if (data_i >= inode->d_blk_cnt)
    iblk_i = data_i - inode->d_blk_cnt;

  for (;
       iblk_i < inode->i_blk_cnt &&
       fds[fd].offset < end;
       iblk_i++)
  {
    adress = (void*) ((int) kfs_super + (int) inode->i_blks[iblk_i] * KFS_BLK_SZ);
    read_indirect_block(begin,
                        end,
                        KFS_INDIRECT_BLK_CNT * (iblk_i) + inode->d_blk_cnt,
                        fd,
                        adress,
                        buf);
  }
}

ssize_t
read(int fd, void *buf, size_t count)
{
  SYSCALL_FAIL(fd < 0 || fd > 256 || !fds[fd].inode) // Invalid fd

  if (fds[fd].offset + count > fds[fd].inode->file_sz)
    count = fds[fd].inode->file_sz - fds[fd].offset;

  read_i_node(fds[fd].offset, fds[fd].offset + count, fd, fds[fd].inode, buf);

  SYSCALL_RETURN((ssize_t) count);
}

off_t
seek(int fd, off_t offset, int whence)
{
  t_uint32 f_offset = 0;
  SYSCALL_FAIL(fd < 0 || fd > 256 || !fds[fd].inode) // Invalid fd

  switch (whence)
  {
  case SEEK_SET:
    f_offset = offset;
    break;
  case SEEK_CUR:
    f_offset += offset;
    break;
  case SEEK_END:
    f_offset = fds[fd].inode->file_sz + offset;
    break;
  }

  SYSCALL_FAIL(f_offset > fds[fd].inode->file_sz) // Invalid offset

  fds[fd].offset = f_offset;
  SYSCALL_RETURN(f_offset);
}

int
close(int fd)
{
  SYSCALL_FAIL(!fds[fd].inode) // Invalid fd
  memset(&(fds[fd]), 0, sizeof (struct kfs_descriptor));
  SYSCALL_RETURN(0);
}
