#include "pic.h"

void
init_ocw(int mask, int port)
{
  int in_mask = 0;

  in_mask = inb(port);
  in_mask |= mask;
  outb(port, in_mask);
}

void
init_pic(int offset1, int offset2)
{
  // ICW1
  outb(PIC1A, 0x11);
  outb(PIC2A, 0x11);

  __asm__ volatile (
    "jmp .1\n\t"
    ".1:\n\t");

  // ICW2
  outb(PIC1B, offset1);
  outb(PIC2B, offset2);

  __asm__ volatile (
    "jmp .2\n\t"
    ".2:\n\t");

  // ICW3
  outb(PIC1B, 4);
  outb(PIC2B, 2);

  __asm__ volatile (
    "jmp .3\n\t"
    ".3:\n\t");

  // ICW4
  outb(PIC1B, 0x1);
  outb(PIC2B, 0x1);

  __asm__ volatile (
    "jmp .4\n\t"
    ".4:\n\t");

  init_ocw(0xFC, PIC1B);
  init_ocw(0xFF, PIC2B);
}
