#include "time.h"
#include "kstd.h"
#include "serial_driver.h"
#include "idt.h"
#include "asm_utils.h"

t_uint32 timer_ticks = 0;

void
init_pit(int hz)
{
  int divider = 1193180 / hz;
  outb(0x43, 0x34);           // binary, mode 2, lo/hi, 0
  outb(0x40, divider & 0xFF); // lobyte
  outb(0x40, divider >> 8);   // hibyte
}

void
timer_hdl(void)
{
  ++timer_ticks;
  ack();
}

unsigned long
gettick(void)
{
  int t = 10 * timer_ticks;
  return t;
}

void
wait(int s)
{
  t_uint32 eticks = timer_ticks + + 100 * s;
  while(timer_ticks < eticks);
}
