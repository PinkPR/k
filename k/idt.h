#ifndef IDT_H
# define IDT_H

# include "macros.h"

# define ACK  0x20

void idt_handler(void);
void idt_handler_0(void);
void idt_handler_1(void);
void idt_handler_2(void);
void idt_handler_3(void);
void idt_handler_4(void);
void idt_handler_5(void);
void idt_handler_6(void);
void idt_handler_7(void);
void idt_handler_8(void);
void idt_handler_9(void);
void idt_handler_10(void);
void idt_handler_11(void);
void idt_handler_12(void);
void idt_handler_13(void);
void idt_handler_14(void);
void idt_handler_15(void);
void kbd_hdl_asm(void);
void timer_hdl_asm(void);
void syscall_hdl_asm(void);

struct idtr
{
  u16 limit;
  u32 base;
} __attribute__ ((packed));

struct idtdsc
{
  u16 base_low;
  u16 segment;
  u8  zero;
  u8  type;
  u16 base_high;
} __attribute__ ((packed));

void ack();
void init_idt(void);

#endif /* IDT_H */
