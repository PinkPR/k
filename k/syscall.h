#ifndef SYSCALL_H
# define SYSCALL_H

# include "write.h"
# include "getkey.h"
# include "kstd.h"

extern unsigned int syscall_tbl[];

void init_syscall_tbl(void);

#endif /* SYSCALL_H */
