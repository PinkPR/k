#include "gdt2.h"

struct gdtdsc gdtd[5];
struct gdtr   gdtreg;

void init_gdtdsc(struct gdtdsc *gdt_dsc,
                 u32 base,
                 u32 limit,
                 u8  ring,
                 u8  desc_type,
                 u8  acces)
{
  gdt_dsc->lim_low    = limit & 0xFFFF;
  gdt_dsc->base_low   = base & 0xFFFF;
  gdt_dsc->base_mid   = (base >> 16) & 0xFF;
  gdt_dsc->access     = (1 << 7) + (ring << 5) + (desc_type << 4) + acces;
  gdt_dsc->gran       = (1 << 7) +
                        (1 << 6) +
                        (0 << 5) +
                        (0 << 4) +
                        ((limit >> 16) & 0xF);
  gdt_dsc->base_high  = base >> 24;
}

void init_gdt(void)
{
  init_gdtdsc(&(gdtd[0]), 0, 0, 0, 0, 0);
  init_gdtdsc(&(gdtd[1]), 0, 0xFFFFFF, 0, 1, 15);   // Kernel Code segment
  init_gdtdsc(&(gdtd[2]), 0, 0xFFFFFF, 0, 1, 7);    // Kernel Data segment
  init_gdtdsc(&(gdtd[3]), 0, 0xFFFFFF, 3, 1, 15);   // Userland Code segment
  init_gdtdsc(&(gdtd[4]), 0, 0xFFFFFF, 3, 1, 7);    // Userland Data segment

  gdtreg.limit = (sizeof (struct gdtdsc) * 5) - 1;
  gdtreg.base  = (u32) gdtd;

  __asm__ volatile (
    "lgdtl %0\n\t"
    "movl %%cr0, %%eax\n\t"
    "orl $1, %%eax\n\t"
    "movl %%eax, %%cr0\n\t"
    "movl $0x10, %%eax\n\t"
    "movw %%ax, %%ds\n\t"
    "movw %%ax, %%es\n\t"
    "movw %%ax, %%fs\n\t"
    "movw %%ax, %%gs\n\t"
    "movw %%ax, %%ss\n\t"
    "ljmp $0x8, $flush\n\t"
    "flush:\n\t"
    :
    : "m"(gdtreg)
    : "memory");
}
