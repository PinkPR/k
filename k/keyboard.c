#include "keyboard.h"

void
kbd_buff_init(struct kbd_buff *buff)
{
  buff->size = KBD_BUFF_SIZE;
  buff->mark = 0;
}

void
kbd_buff_push(struct kbd_buff *buff, int code)
{
  if (buff->mark == buff->size)
    return;

  buff->buff[buff->mark] = code;
  buff->mark++;
}

int
kbd_buff_pop(struct kbd_buff *buff)
{
  if (buff->mark == 0)
    return -1;

  int code = buff->buff[0];

  for (int i = 0; i < buff->size - 1; i++)
    buff->buff[i] = buff->buff[i + 1];

  buff->mark--;

  return code;
}

void
kbd_hdl(void)
{
  ack();
  int i = inb(KBD_BUFF_PORT);

  if (i - 0x80 < 0)
    kbd_buff_push(&buffer, i);
}
