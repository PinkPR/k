#ifndef GDT2_H
# define GDT2_H

# include "macros.h"

struct gdtr
{
  u16 limit;
  u32 base;
} __attribute__ ((packed));

struct gdtdsc
{
  u16 lim_low;
  u16 base_low;
  u8  base_mid;
  u8  access;
  u8  gran;
  u8  base_high;
} __attribute__ ((packed));

void init_gdt(void);

#endif /* GDT2_H */
