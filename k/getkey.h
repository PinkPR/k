#ifndef GETKEY_H
# define GETKEY_H

# include "keyboard.h"

extern struct kbd_buff buffer;

int getkey(void);

#endif /* GETKEY_H */
