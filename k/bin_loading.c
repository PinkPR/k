#include "bin_loading.h"
#include "kfs.h"
#include <stdio.h>
#include "sbrk.h"

Elf32_Ehdr
get_elf_header(int fd)
{
  Elf32_Ehdr header;

  seek(fd, 0, SEEK_SET);
  read(fd, &header, sizeof (Elf32_Ehdr));

  return header;
}

Elf32_Phdr
get_program_header(int fd,
                   Elf32_Ehdr elf_header,
                   int n)
{
  Elf32_Phdr header;

  seek(fd, elf_header.e_phoff + n * sizeof (Elf32_Phdr),
       SEEK_SET);
  read(fd, &header, sizeof (Elf32_Phdr));

  return header;
}

void
load_and_run_elf(int fd)
{
  Elf32_Ehdr elf_header;
  Elf32_Phdr p_header;

  elf_header = get_elf_header(fd);

  for (int i = 0; i < elf_header.e_phnum; i++)
  {
    p_header = get_program_header(fd, elf_header, i);
    seek(fd, p_header.p_offset, SEEK_SET);
    read(fd, (void*) p_header.p_vaddr, p_header.p_memsz);

    if (!(p_header.p_flags & PF_X))
      brk = (char*) (p_header.p_vaddr + p_header.p_memsz + 1);
  }

  __asm__ volatile (
    "jmp %%eax\n\t"
    : : "a"(elf_header.e_entry));
}
