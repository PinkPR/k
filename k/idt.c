#include "idt.h"
#include "write.h"
#include "pic.h"
#include "serial_driver.h"
#include "keyboard.h"
#include "time.h"
#include "stdio.h"

struct idtdsc   idtd[256];
struct idtr     idtreg;

void
ack(void)
{
  outb(PIC1A, ACK);
  outb(PIC2A, ACK);
}

#define HANDLER(NUM) void                             \
  handler_##NUM(void)                                 \
  {                                                   \
    printf("Error %d\n", NUM);                        \
  }

HANDLER(0)
HANDLER(1)
HANDLER(2)
HANDLER(3)
HANDLER(4)
HANDLER(5)
HANDLER(6)
HANDLER(7)
HANDLER(8)
HANDLER(9)
HANDLER(10)
HANDLER(11)
HANDLER(12)
HANDLER(13)
HANDLER(14)
HANDLER(15)


void
handler(void)
{
  write("Error", 5);
  ack();
  inb(0x60);
}

void
init_idtdsc(struct idtdsc* idt_dsc,
            u32 base,
            u16 segment,
            u8  ring)
{
  idt_dsc->base_low   = base & 0xFFFF;
  idt_dsc->segment    = segment;
  idt_dsc->zero       = 0;
  idt_dsc->type       = (1 << 7) |
                        (ring << 5) |
                        0xE;
  idt_dsc->base_high  = base >> 16;
}

void
init_special_handlers(void)
{
  init_idtdsc(&idtd[0x40], (u32) timer_hdl_asm, 0x8, 0);    // TIMER
  init_idtdsc(&idtd[0x41], (u32) kbd_hdl_asm, 0x8, 0);      // KEYBOARD
  init_idtdsc(&idtd[0x80], (u32) syscall_hdl_asm, 0x8, 0);  // SYSCALLS
}

void
init_idt(void)
{
    init_idtdsc(&(idtd[0]), (u32) idt_handler_0, 0x8, 0);
    init_idtdsc(&(idtd[1]), (u32) idt_handler_1, 0x8, 0);
    init_idtdsc(&(idtd[2]), (u32) idt_handler_2, 0x8, 0);
    init_idtdsc(&(idtd[3]), (u32) idt_handler_3, 0x8, 0);
    init_idtdsc(&(idtd[4]), (u32) idt_handler_4, 0x8, 0);
    init_idtdsc(&(idtd[5]), (u32) idt_handler_5, 0x8, 0);
    init_idtdsc(&(idtd[6]), (u32) idt_handler_6, 0x8, 0);
    init_idtdsc(&(idtd[7]), (u32) idt_handler_7, 0x8, 0);
    init_idtdsc(&(idtd[8]), (u32) idt_handler_8, 0x8, 0);
    init_idtdsc(&(idtd[9]), (u32) idt_handler_9, 0x8, 0);
    init_idtdsc(&(idtd[10]), (u32) idt_handler_10, 0x8, 0);
    init_idtdsc(&(idtd[11]), (u32) idt_handler_11, 0x8, 0);
    init_idtdsc(&(idtd[12]), (u32) idt_handler_12, 0x8, 0);
    init_idtdsc(&(idtd[13]), (u32) idt_handler_13, 0x8, 0);
    init_idtdsc(&(idtd[14]), (u32) idt_handler_14, 0x8, 0);
    init_idtdsc(&(idtd[15]), (u32) idt_handler_15, 0x8, 0);

  for (int i = 16; i <= 255; i++)
    init_idtdsc(&(idtd[i]), (u32) idt_handler, 0x8, 0);

  init_special_handlers();

  idtreg.limit = (sizeof (struct idtdsc) * 256) - 1;
  idtreg.base  = (u32) idtd;

  __asm__ volatile (
    "sti\n\t"
    "lidt %0\n\t"
    :
    : "m"(idtreg)
    : "memory");
}
