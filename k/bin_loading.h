#ifndef BIN_LOADING_H
# define BIN_LOADING_H

# include "elf.h"
# include "kfs.h"

Elf32_Ehdr get_elf_header(int fd);
Elf32_Phdr get_program_header(int fd,
                              Elf32_Ehdr elf_header,
                              int n);
void load_and_run_elf(int fd);

#endif /* BIN_LOADING_H */
